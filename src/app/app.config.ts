import {
  APP_INITIALIZER,
  ApplicationConfig,
  inject,
  Injector,
} from '@angular/core';
import { provideRouter } from '@angular/router';
import { appRoutes } from './app.routes';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideStore, Store } from '@ngrx/store';
import { providerDbFeature } from '../../libs/db/src/lib/providers/providerDbFeature';
import { provideHttpClient } from '@angular/common/http';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(appRoutes),
    provideHttpClient(),
    provideAnimationsAsync(),
    provideStore(),
    providerDbFeature(),
  ],
};
