import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ENVIRONMENT } from '@bjj-tips/environments';
import { ShellComponent } from '@bjj-tips/shell';

@Component({
  standalone: true,
  imports: [RouterModule, ShellComponent],
  selector: 'bjj-tips-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  title = 'bjj-tips';
  env = ENVIRONMENT;
}
