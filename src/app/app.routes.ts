import { Route } from '@angular/router';

export const appRoutes: Route[] = [
  {
    path: '**',
    loadChildren: () =>
      import('@bjj-tips/catalog').then((module) => module.catalogRoutes),
  },
];
