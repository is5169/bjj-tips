export { catalogRoutes } from './lib/routing/catalog.routes';

export { CatalogComponent } from './lib/components/catalog/catalog.component';
