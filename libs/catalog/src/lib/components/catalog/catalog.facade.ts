import { inject, Injectable, TemplateRef } from '@angular/core';
import { Store } from '@ngrx/store';
import { dbFeature, Post } from '@bjj-tips/db';
import { ComponentStore } from '@ngrx/component-store';
import { map, startWith, switchMap, tap } from 'rxjs';
import { concatLatestFrom } from '@ngrx/operators';
import {
  KbqSidepanelPosition,
  KbqSidepanelService,
  KbqSidepanelSize,
} from '@koobiq/components/sidepanel';

interface CatalogState {
  selectedTags: string[];
  tags: string[];
  isSidePanelOpened: boolean;
}

@Injectable()
export class CatalogFacade extends ComponentStore<CatalogState> {
  private readonly store = inject(Store);
  private readonly sidePanelService = inject(KbqSidepanelService);

  constructor() {
    super({ selectedTags: [], tags: [], isSidePanelOpened: false });
  }

  readonly selectedTags$ = this.select((state) => state.selectedTags);
  readonly tags$ = this.select((state) => state.tags);

  readonly isSidePanelOpened$ = this.select((state) => state.isSidePanelOpened);

  private readonly videos$ = this.select(
    this.selectedTags$,
    this.store.select(dbFeature.selectPosts),
    (filters, videos) => this.filterVideosByTags(videos, filters)
  );

  readonly viewModel$ = this.select({
    videos: this.videos$,
    tags: this.tags$,
    isSidePanelOpened: this.isSidePanelOpened$,
  });

  readonly selectTags = this.effect<CatalogState['selectedTags']>((tags$) => {
    return tags$.pipe(tap((tags) => this.patchState({ selectedTags: tags })));
  });

  readonly filterTags = this.effect<string>((value$) => {
    return value$.pipe(
      startWith(''),
      concatLatestFrom(() => [
        this.store.select(dbFeature.selectTags),
        this.selectedTags$,
      ]),
      map(([textFilter, tags, selectedTags]) => {
        const filteredTags = this.filterTagsByName(tags, textFilter);

        this.patchState({
          tags: this.moveSelectedTagsFirst(filteredTags.sort(), selectedTags),
        });
      })
    );
  });

  readonly popSelectedTags = this.effect<string[]>((target$) => {
    return target$.pipe(
      switchMap(() => this.selectedTags$),
      concatLatestFrom(() => this.tags$),
      map(([selectedTags, tags]) => {
        this.patchState({
          tags: this.moveSelectedTagsFirst(tags.sort(), selectedTags),
        });
      })
    );
  });

  readonly toggleSidePanel = this.effect<TemplateRef<unknown>>((templateRef$) =>
    templateRef$.pipe(
      concatLatestFrom(() => this.isSidePanelOpened$),
      tap(([template, isOpened]) => {
        if (!isOpened) {
          const sidePanel = this.sidePanelService.open(template, {
            position: KbqSidepanelPosition.Left,
            hasBackdrop: false,
            size: KbqSidepanelSize.Small,
          });

          this.closeSidePanel(sidePanel.afterClosed());

          this.patchState({ isSidePanelOpened: true });
        } else {
          this.sidePanelService.closeAll();
        }
      })
    )
  );

  readonly closeSidePanel = this.effect<void>((trigger$) =>
    trigger$.pipe(tap(() => this.patchState({ isSidePanelOpened: false })))
  );

  private filterTagsByName(tags: string[], textFilter: string): string[] {
    if (!textFilter) {
      return tags;
    }

    return tags.filter((option) =>
      option.toLowerCase().includes(textFilter.toLowerCase())
    );
  }

  private moveSelectedTagsFirst(
    tags: string[],
    selectedTags: string[]
  ): string[] {
    const unselected = tags
      .filter((option) => !selectedTags.includes(option))
      .sort((a, b) => tags.indexOf(a) - tags.indexOf(b));

    return selectedTags.concat(unselected);
  }

  private filterVideosByTags(videos: Post[], tags: string[]): Post[] {
    if (!tags.length) {
      return videos;
    }

    return videos.filter((video) =>
      tags.every((filter) => video.tags.includes(filter))
    );
  }
}
