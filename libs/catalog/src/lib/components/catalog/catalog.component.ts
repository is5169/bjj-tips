import {
  ChangeDetectionStrategy,
  Component,
  inject,
  OnInit,
  TemplateRef,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { CatalogFacade } from './catalog.facade';
import { KbqSelectModule } from '@koobiq/components/select';
import { KbqCheckboxModule } from '@koobiq/components/checkbox';
import { NonNullableFormBuilder, ReactiveFormsModule } from '@angular/forms';
import { KbqInputModule } from '@koobiq/components/input';
import {
  CdkFixedSizeVirtualScroll,
  CdkVirtualForOf,
  ScrollingModule,
} from '@angular/cdk/scrolling';
import { VideoComponent } from '../video/video.component';
import { Post } from '@bjj-tips/db';
import { KbqSidepanelModule } from '@koobiq/components/sidepanel';
import { KbqListModule } from '@koobiq/components/list';
import { KbqButtonModule } from '@koobiq/components/button';

@Component({
  selector: 'lib-catalog',
  standalone: true,
  imports: [
    CommonModule,
    KbqSelectModule,
    KbqCheckboxModule,
    KbqInputModule,
    ReactiveFormsModule,
    CdkVirtualForOf,
    CdkFixedSizeVirtualScroll,
    VideoComponent,
    ScrollingModule,
    KbqSidepanelModule,
    KbqListModule,
    KbqButtonModule,
  ],
  templateUrl: './catalog.component.html',
  styleUrl: './catalog.component.scss',
  providers: [CatalogFacade],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CatalogComponent implements OnInit {
  private catalogFacade = inject(CatalogFacade);
  private formBuilder = inject(NonNullableFormBuilder);

  public readonly virtualScrollItemHeight = 264;

  protected searchControl = this.formBuilder.control('');
  protected selected = this.formBuilder.control([] as string[]);

  viewModel$ = this.catalogFacade.viewModel$;

  ngOnInit(): void {
    this.catalogFacade.filterTags(this.searchControl.valueChanges);
    this.catalogFacade.selectTags(this.selected.valueChanges);
    this.catalogFacade.popSelectedTags(this.selected.valueChanges);
  }

  trackByVideoId(_index: number, video: Post): number {
    return video.id;
  }

  toggleSidePanel(template: TemplateRef<unknown>): void {
    this.catalogFacade.toggleSidePanel(template);
  }
}
