import {
  ChangeDetectionStrategy,
  Component,
  inject,
  Input,
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { Post } from '@bjj-tips/db';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'lib-video',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './video.component.html',
  styleUrl: './video.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class VideoComponent {
  private domSanitizer = inject(DomSanitizer);

  @Input({ required: true }) video!: Post;
  @Input({ required: true }) height!: number;

  getVideoUrl(): SafeUrl {
    return this.domSanitizer.bypassSecurityTrustResourceUrl(
      `https://www.youtube.com/embed/${this.video.videoId}`
    );
  }
}
