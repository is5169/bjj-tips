import { Route } from '@angular/router';

export const catalogRoutes: Route[] = [
  {
    path: '',
    loadComponent: () =>
      import('../components/catalog/catalog.component').then(
        (module) => module.CatalogComponent
      ),
  },
];
