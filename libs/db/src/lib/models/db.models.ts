export interface Db {
  posts: Post[];
}

export interface Post {
  videoId: string;
  tags: string[];
  id: number;
}
