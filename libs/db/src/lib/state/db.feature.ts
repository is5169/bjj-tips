import { createFeature, createReducer, createSelector, on } from '@ngrx/store';
import { LoadingState } from '@bjj-tips/state';
import { DbActions } from './db.actions';
import { Post } from '../models/db.models';

export const initialState = {
  posts: [] as Post[],
  loadingState: LoadingState.INIT,
};

export const dbFeature = createFeature({
  name: 'db',
  reducer: createReducer(
    initialState,
    on(DbActions.loaded, (state, { posts }) => {
      return {
        ...state,
        posts,
        loadingState: LoadingState.LOADED,
      };
    })
  ),
  extraSelectors: ({ selectPosts }) => ({
    selectTags: createSelector(selectPosts, (posts) => {
      let tags: Post['tags'] = [];

      const postsQuantity = posts.length;

      for (let i = 0; i < postsQuantity; i++) {
        tags = Array.prototype.concat.apply(tags, posts[i].tags);
      }

      const tagsSet = new Set(tags);

      return [...tagsSet].sort();
    }),
  }),
});
