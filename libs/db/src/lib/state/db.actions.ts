import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { Post } from '../models/db.models';

export const DbActions = createActionGroup({
  source: 'DB Init',
  events: {
    loaded: props<{ posts: Post[] }>(),
    init: emptyProps(),
  },
});
