import { inject } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { DbActions } from './db.actions';
import { map, switchMap } from 'rxjs';
import { DbApiService } from '../services/db-api.service';

export class DbEffects {
  private actions$ = inject(Actions);
  private dbApiService = inject(DbApiService);

  init = createEffect(() => {
    return this.actions$.pipe(
      ofType(DbActions.init),

      switchMap(() =>
        this.dbApiService
          .getDb()
          .pipe(map((db) => DbActions.loaded({ posts: db.posts })))
      )
    );
  });
}
