import { Observable } from 'rxjs';
import { Db } from '../models/db.models';
import { inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ENVIRONMENT } from '@bjj-tips/environments';

@Injectable({
  providedIn: 'root',
})
export class DbApiService {
  private http = inject(HttpClient);

  getDb(): Observable<Db> {
    return this.http.get<Db>(ENVIRONMENT.dbUrl);
  }
}
