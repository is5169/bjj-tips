import {
  APP_INITIALIZER,
  EnvironmentProviders,
  Injector,
  makeEnvironmentProviders,
} from '@angular/core';
import { provideEffects } from '@ngrx/effects';
import { DbEffects } from '../state/db.effects';
import { provideState, Store } from '@ngrx/store';
import { dbFeature } from '../state/db.feature';
import { DbActions } from '../state/db.actions';
import { filter, take } from 'rxjs';

export function providerDbFeature(): EnvironmentProviders {
  return makeEnvironmentProviders([
    provideState(dbFeature),
    provideEffects(DbEffects),
    providerDbFeatureAppInitializer(),
  ]);
}

function providerDbFeatureAppInitializer(): EnvironmentProviders {
  return makeEnvironmentProviders([
    {
      provide: APP_INITIALIZER,
      multi: true,
      useFactory: (injector: Injector) => () => {
        const store = injector.get(Store);
        store.dispatch(DbActions.init());
        return store.select(dbFeature.selectPosts).pipe(
          filter((items) => !!items.length),
          take(1)
        );
      },
      deps: [Injector],
    },
  ]);
}
